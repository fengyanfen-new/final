# yaml笔记
**YAML是YAML Ain't Markup Language的缩写，即YAML不是置标语言。但是这确实是一种置标语言，和JSON、XML、SDL等类似。YAML是一种简洁、可读性高，用来表达序列化数据的格式。可以用做配置、数据传输等场景。**
## YAML具有的特点：
1. 大小写敏感；
2. 通过缩进表示层级关系；
3. 只能使用空格进行缩进，禁止使用tab缩进；
4. 缩进的空格数没有要求，但是相同层级的数据需要对齐；
5. 使用#作为注释符号（这一点比json有优势）；

---
#### 2.数据类型 ####
* 字符串
```
#YAML
str: abc
```
* 布尔值
布尔值用true和false表示
```
#YAML
isTrue: true
isTrue: false
```
```
#JSON
{
    isTrue:true,
    isTrue:false
}
```
*整数
数值直接以字面形式表示
```
#YAML
int: 10
```
```
#JSON
{
    int:10
}
```
* 浮点数
数值直接以字面形式表示
```
float: 1.23
double: 2.34
```
```#JSON
{
    float:1.23,
    double:2.34
}
```
* Null
null值用~表示
```
#YAML
person: ~
```
```
{
    person:null
```
* 时间

时间采用ISO8601格式表示 
```
#YAML
iso8601: 2018-05-20t10:59:43.10-05:00
```
```
#JSON
{
    iso8601:new Date('2018-05-20t10:59:43.10-05:00')
}
```

日期采用ISO8601的格式yyyy-MM-dd表示
```
#YAML
date: 2018-05-20
```
```
{
    date:new Date('2018-05-20')
}
```
注：YAML允许使用两个感叹号强制转换类型
```
#YAML
str1: !!str 123
str2: !!str true
#JSON
{
      str1:'123',
      str2:'true'
}
```
### 3、数据结构 ###
1. Map，散列表
使用:表示键值对，统一缩进的所有键值对属于一个Map

name: John
age: 18
#### 也可以写在一行
```
{ name: John, age: 18}
#JSON
{
    'name':'John',
    'age': 18
}
```
2、List，数组
使用-来表示数组中的一个元素

#YAML
- a
- b
- c
#也可以写在一行
```
[a, b, c]
#JSON
['a', 'b', 'c']
```
3、scalar，纯量
数据的最小单位，不可再分割

4、数据结构的嵌套
YAML中的数据结构可以相互嵌套，嵌套方式有如下几种：
1、Map嵌套Map
```
#YAML
websites:
 YAML: yaml.org 
 Ruby: ruby-lang.org 
 Python: python.org 
 Perl: use.perl.org 
#JSON
{ websites: 
   { YAML: 'yaml.org',
     Ruby: 'ruby-lang.org',
     Python: 'python.org',
     Perl: 'use.perl.org' } }
```
2、Map嵌套List
```
#YAML
languages:
 - Ruby
 - Perl
 - Python 
 - c
#JSON
{
    languages:[ 
    'Ruby',
    'Perl',
    'Python',
    'c'] 
}
```
3、List嵌套List

```
#YAML
-
  - Ruby
  - Perl
  - Python 
- 
  - c
  - c++
  - java
#或者
- [Ruby,Perl,Python]
- [c,c++,java]
#JSON
[
    [
    'Ruby',
    'Perl',
    'Python'
    ],
    [
    'c',
    'c++',
    'java'
    ]
]
```
4、List嵌套Map
```
#YAML
-
  name: John
  age: 18
- 
  name: Lucy
  age: 16
#JSON
[
    {
