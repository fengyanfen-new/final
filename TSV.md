# 什么是tsv？
*tsv是Tab Separated Values的缩写*

**“制表符分隔值”，而这些制表符分隔值文件被创建和使用的许多电子表格应用程序。制表符分隔值文件的内容可以包括文本，分为行和列的数学，科学或统计数据。制表符分隔这些数据段成列，因此文件格式名“制表符分隔值”。**

**TSV是用制表符tab分隔的文件**

---
| Item       |    Value| Qty |
|:--------|--------:|:--:|
|Computer|1600 USD| 5 |
|Phone    |12 USD|  5|
|Pipe   |   1 USD |234|
## 上述表格用tsv应为
### Item(tab)Value(tab)Qty
### Computer(tab)1600 USD(tab)5
### Phone(tab)12 USD(tab)5
### Pipe(tab)1 USD(tab)234
>注意(tab)为键盘上的tab键