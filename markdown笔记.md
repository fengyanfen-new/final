**Markdown 是一种轻量级标记语言，创始人为 John Gruber。它允许人们「使用易读易写的纯文本格式编写文档，然后转换成有效的 XHTML（或者 HTML）文档」**
# Markdown的优点
1. 其专注于文字内容本身，语法格式简单，易学
2. 纯文本，易读易写，多系统兼容，可以方便地纳入版本控制
3. 语法简单，没有什么学习成本，能轻松在码字的同时做出美观大方的排版
***
*标题的语法*

**标题，只需要在其前面加上#，“#”后面有空格，文字与符号之间要用空格**
# 标题1
## 标题2
### 标题3
#### 标题4
##### 标题5
###### 标题6
---
# 列表分为有序列表，无序列表和嵌套列表
### 无序列表只需要在文字之前加一个“*”即可，注意文字与符号之间也需要有空格
* 列表1
* 列表2
* 列表3

#### 有序列表表只需要前面写1.即可，注意1后面应有一点，以及一个空格，编译器会自动给它们编号。一般的编辑工具，在回车换行之后编译器也会自动给它们编号
1. 列表1
2. 列表2
3. 列表3

#### 无序和有序列表、无序列表之间以及有序列表之间都可以嵌套，注意这里的嵌套列表，需要缩进四个空格
* 列表1
    1. sub item1
    2. sub item2
    3. sub item3
***
*使用>来表示引用，在实际使用中，你可以只在第一行写上>，后面的行可以省略。直到碰到空行，这个引用会一直有效。*
 >人的一生都是在不断跌倒不断站起来！
***

## 加粗的语法如下，在你要加粗的字体前后加两个**即可，不需要加空格
**加粗字体**
---
## 斜体的语法就是在字体前后加一个*即可
*斜体字*
## 删除线的语法在字体前后加~~
~~加删除线的字体~~
---
### 链接的格式如下，用[]把文字括起来，后面的链接放在()里面即可
[深度学习与资源共享公众号资源汇总](https：//dwz.cn/kCJNRBl9)

## 注：链接的url后面可以跟title，格式为文字，url和title之间有一个空格，title需要用引号或者小括号包起来。

### 分割线
> 用三个*或者-
***
---
### 表格的语法
| Item       |    Value| Qty |
|:--------|--------:|:--:|
|Computer|1600 USD| 5 |
|Phone    |12 USD|  5|
|Pipe   |   1 USD |234|
### 插入图片
![中山大学南方学院](https://images.gitee.com/uploads/images/2019/1216/171221_ae2b5cfa_5371810.jpeg)
#### 流程图
###### 自上而下的顺序

```
graph TB
A-->B
 style B fill:#ccf,stroke:#f66,stroke-width:2px,stroke-dasharray: 10,5
```
###### 自下而上的顺序
```
graph BT
A-->B
 style A fill:#f9f,stroke:#333,stroke-width:4px,fill-opacity:0.5
```
